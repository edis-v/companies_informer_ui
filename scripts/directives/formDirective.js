(function () {

    var formDirective = function () {

        var directiveCtrl = function () {

            $scope.callFunc = function (object) {
                $scope.func().(object)
            };

        };

        return {
            restrict: 'E',
            scope: {
                func: '&'
            },
            controller: directiveCtrl,
            templateUrl: function(elem,attrs) {
                return attrs.template || 'some/path/default.html'
            }

        };
    };

    //<form-directive func="functionM(object)" template="new.html"></form-directive> PRIMJER POZIVANJA

    APP.directive('formDirective', formDirective);
}());