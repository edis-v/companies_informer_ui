(function () {

    var loginCtrl = function ($scope, crud_service) {

        $scope.login = function () {
            var data = {"access": $scope.user };
            crud_service.post_item('access', data).then(function (success) {
                console.log("Login success", success);
            }, function (error) {
                console.log("Login error", error);
            });
        };
    };

    APP.controller('loginCtrl',loginCtrl);
}());

