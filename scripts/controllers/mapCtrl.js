(function () {

    var mapCtrl = function ($scope) {

        //start google maps

        function initialize() {
            var mapProp = {
                center: new google.maps.LatLng($scope.company.gmap_x, $scope.company.gmap_y),
                zoom: 5,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            $scope.map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

            var marker = new google.maps.Marker({
                map: $scope.map,
                position: new google.maps.LatLng($scope.company.gmap_x, $scope.company.gmap_y),
                title: 'my title'
            });
            var infowindow = new google.maps.InfoWindow({
                content: "We are here!"
            });
            google.maps.event.addListener(marker, 'click', function () {
                $scope.map.setZoom(9);
                $scope.map.setCenter(marker.getPosition());
            });
            infowindow.open($scope.map, marker);
        }
            initialize();
            console.log("initialization called");
            //end

    };

    APP.controller('mapCtrl', mapCtrl);
}());