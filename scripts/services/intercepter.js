(function(){

    var intercepter = function($q){
        return {
            request: function(config){
                config.headers = {
                    'Authorization' : 'Token token=' + localStorage.getItem('token'),
                    'Content-Type' : 'application/json'
                };
                return config;
            },
            response: function(result){
                return result;
            },

            responseError: function(rejection) {
                console.log('Failed with',rejection.status, 'status');
                return $q.reject(rejection);
            }
        }
    };


    APP.service('intercepter',intercepter);
}());