(function () {

    var crud_service = function ($http) {

        //var host = 'https://companies-informer-service.herokuapp.com/api/v1/';
        var host = 'http://0.0.0.0:3000/api/v1/';

        this.get_item = function(record){
            var request = $http({
                method: 'get',
                url: host+record
            });
            return request;
        };

        this.post_item = function(source, data) {
            var request = $http({
                method: 'post',
                url: host+source,
                data: data
            });
            return request;
        };

        this.delete_item = function(source){
            var request = $http({
                method: 'delete',
                url: host+source
            });
            return request;
        };

        this.put_item = function(source,data){
            var request = $http({
                method: 'put',
                url: host+source,
                data: data
            });
            return request;
        };


    };

    APP.service('crud_service',crud_service);
}());