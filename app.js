var APP = angular.module('informerApp', ['ui.router']);

APP.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

    $urlRouterProvider.otherwise('/login');

    $stateProvider
        .state('login',{
            url: '/login',
            templateUrl: 'views/layouts/loginPage.html',
            controller: 'loginCtrl'
        })
        .state('root',{
            url: '/rootAdmin',
            //resolve: {
            //    user: function (crud_service) {
            //        return crud_service.get_item('users', window.localStorage.getItem('id'));
            //    }
            //},
            templateUrl: 'views/layouts/rootAdminLayout.html',
            controller: 'rootAdminCtrl'
        })
        .state('root.company',{
            url: '/company/:id',
            resolve: {
                company: function (crud_service, $stateParams) {
                    return crud_service.get_item('companies/'+$stateParams.id);
                }
            },
            templateUrl: 'views/layouts/companyProfile.html',
            controller: 'companyCtrl'
        })
        .state('root.company.profile', {
            url: '/details',
            templateUrl: 'views/partials/companyDetails.html',
            controller: 'mapCtrl'
        });

    $httpProvider.interceptors.push('intercepter');
});

